package com.ljh.service.impl;

import com.ljh.entity.Type;
import com.ljh.mapper.TypeMapper;
import com.ljh.service.TypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;


@Service
public class TypeServiceImpl extends ServiceImpl<TypeMapper, Type> implements TypeService {

}
