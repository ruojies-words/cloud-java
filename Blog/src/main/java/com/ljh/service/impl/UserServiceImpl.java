package com.ljh.service.impl;

import com.ljh.entity.User;
import com.ljh.mapper.UserMapper;
import com.ljh.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
