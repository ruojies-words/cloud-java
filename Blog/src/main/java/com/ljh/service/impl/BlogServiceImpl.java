package com.ljh.service.impl;

import com.ljh.entity.Blog;
import com.ljh.mapper.BlogMapper;
import com.ljh.service.BlogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;


@Service
public class BlogServiceImpl extends ServiceImpl<BlogMapper, Blog> implements BlogService {

}
