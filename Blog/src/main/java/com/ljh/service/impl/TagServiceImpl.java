package com.ljh.service.impl;

import com.ljh.entity.Tag;
import com.ljh.mapper.TagMapper;
import com.ljh.service.TagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;


@Service
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag> implements TagService {

}
