package com.ljh.service;

import com.ljh.entity.Type;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 关注公众号：MarkerHub
 * @since 2021-07-24
 */
public interface TypeService extends IService<Type> {

}
