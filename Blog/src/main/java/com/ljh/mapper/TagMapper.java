package com.ljh.mapper;

import com.ljh.entity.Tag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


public interface TagMapper extends BaseMapper<Tag> {

}
