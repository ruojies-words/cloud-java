package com.ljh.mapper;

import com.ljh.entity.Type;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


public interface TypeMapper extends BaseMapper<Type> {

}
