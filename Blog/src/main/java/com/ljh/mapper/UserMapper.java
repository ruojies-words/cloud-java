package com.ljh.mapper;

import com.ljh.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


public interface UserMapper extends BaseMapper<User> {

}
