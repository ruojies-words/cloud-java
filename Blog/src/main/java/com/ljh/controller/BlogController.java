package com.ljh.controller;



import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ljh.common.lang.Result;
import com.ljh.entity.Blog;
import com.ljh.entity.Type;
import com.ljh.service.BlogService;
import com.ljh.util.ShiroUtil;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;


@RestController
public class BlogController {
    @Autowired
    BlogService blogService;



    @GetMapping("/index")
    public Result list(@RequestParam(defaultValue = "1") Integer currentPage) {

        Page page = new Page(currentPage, 5);
        IPage pageData = blogService.page(page, new QueryWrapper<Blog>().orderByDesc("created"));

        return Result.succ(pageData);
    }

    @PostMapping("/index2")
    public Result list2(@RequestParam(defaultValue = "1") Integer currentPage, @Validated @RequestBody Blog blog) {

        Page page = new Page(currentPage, 15);
        IPage pageData = blogService.page(page, new QueryWrapper<Blog>().like("title", blog.getTitle()));

        return Result.succ(pageData);
    }

    @GetMapping("/blog/{id}")
    public Result detail(@PathVariable(name = "id") Long id) {
        Blog blog = blogService.getById(id);
        Assert.notNull(blog, "该博客已被删除");
        return Result.succ(blog);
    }

    @RequiresAuthentication
    @PostMapping("/blog/edit")
    public Result edit(@Validated@RequestBody Blog blog) {
        Blog temp = null;
        if(blog.getId() != null) {
            temp = blogService.getById(blog.getId());
            Assert.isTrue(temp.getUserId().equals(ShiroUtil.getProfile().getId()), "没有权限");
        } else {
            temp = new Blog();
            temp.setUserId(ShiroUtil.getProfile().getId());
            temp.setCreated(LocalDateTime.now());
            temp.setStatus(0);
        }
        BeanUtil.copyProperties(blog, temp,  "id", "userId", "created", "status");
        blogService.saveOrUpdate(temp);
        return Result.succ(null);
    }

    @RequiresAuthentication
    @PostMapping("/blog/add")
    public Result add(@Validated @RequestBody Blog blog) {


        System.out.println("id=" + blog.getId());
        Blog temp = null;
        if(blog.getId() != null) {
            temp = blogService.getById(blog.getId());
            temp.setEyesights(temp.getEyesights()+1);
            System.out.println("temp="+temp);
            blogService.saveOrUpdate(temp);
            return Result.succ(null);
        } else {
            return Result.fail("error");
        }
    }

    @PostMapping("/blog/addcount")
    public Result addcount(@Validated @RequestBody Blog blog) {


        System.out.println("id=" + blog.getId());
        Blog temp = null;
        if(blog.getId() != null) {
            temp = blogService.getById(blog.getId());
            temp.setEyesights(temp.getEyesights()+1);
            System.out.println("temp="+temp);
            blogService.saveOrUpdate(temp);
            return Result.succ(null);
        } else {
            return Result.fail("error");
        }
    }



    @PostMapping("/delete/{id}")
    @ResponseBody
    public Result delete(@PathVariable(name = "id") Long id){
        boolean result = blogService.removeById(id);
        if(result == true){
            return Result.succ(null);

        }else {
            return Result.fail(null);
        }
    }

    @PostMapping("/blog/search")
    public Result Search(@Validated @RequestBody Blog blog) {

        Page page = new Page(1, 500);
        IPage pageData = blogService.page(page, new QueryWrapper<Blog>().like("title", blog.getTitle()));

        return Result.succ(pageData);
    }



    @PostMapping("/blog/searchbytype")
    public Result Searchbytype(@RequestParam(defaultValue = "1") Integer currentPage, @Validated @RequestBody Blog blog) {
        System.out.println(blog.getTypes());
        Page page = new Page(currentPage, 10);
        IPage pageData = blogService.page(page, new QueryWrapper<Blog>().eq("types", blog.getTypes()));

        return Result.succ(pageData);
    }

    @PostMapping("/blog/searchbytag")
    public Result Searchbytag(@RequestParam(defaultValue = "1") Integer currentPage, @Validated @RequestBody Blog blog) {
        System.out.println(blog.getTags());
        Page page = new Page(currentPage, 10);
        IPage pageData = blogService.page(page, new QueryWrapper<Blog>().like("tags", blog.getTags()));
        System.out.println(pageData);
        return Result.succ(pageData);
    }








}
